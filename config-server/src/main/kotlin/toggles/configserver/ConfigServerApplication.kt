package toggles.configserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.config.server.EnableConfigServer

fun main(args: Array<String>) {
	runApplication<ConfigServerApplication>(*args)
}

@SpringBootApplication
@EnableConfigServer
class ConfigServerApplication