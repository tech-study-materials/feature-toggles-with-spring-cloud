<h1>How to demo it:</h1>

<h2>prepare artifacts:</h2>
* `gradlew build jibDockerBuild`

<h2>set up gitlab:</h2>
* `docker compose up gitlab`
  * takes quite long to startup, so sample-apps wouldn't start yet
  * wait a minute until `gitlab` fully starts (observe logs)
* gitlab is reachable @ [http://localhost:80](http://localhost:80)
* login to `gitlab`
  * username: 'root'
  * password: 'secret123'
  * go to http://localhost/admin/application_settings/network
    * expand `Outbound requests`
    * check `Allow requests to the local network from webhooks and integrations`
    * put `config-server-prod, config-server-staging` in `Local IP addresses and domain names that hooks and integrations can access` box
    * save changes 
  * create public group `configuration`
  * create public subgroup `staging` in group `configuration`
  * create public repo `sample-app` in group `staging`
  * in root of the repo, create `sample-app.yml` with the following content:
  ```yaml
  features:
    greetings: angry
  ```
  * add webhook (on push events, no ssl) to endpoint `http://config-server-staging:8080/monitor`
  * repeat for subgroup `prod`, but with `features:greetings: texan` and webhook to `config-server-prod`

<h2>deploy everything else:</h2>
* `docker compose up -d`
* apps' instances are reachable @ 
  * [http://localhost:8080/prod](http://localhost:8080/prod)
  * [http://localhost:8080/staging](http://localhost:8080/staging)

<h2>demo script:</h2>
* open prod page, check the greeting
* change value of `features.greetings` in {gitlab}/configuration/prod/sample-app/-/blob/main/sample-app.yml
* commit your change
* refresh prod page, the greeting should have changed in few seconds
* repeat for staging apps & configuration

[diagram](http://www.plantuml.com/plantuml/svg/dPDBRuCm48Jl_XKhBZqbt56KSo-LMYd7ICZ00bQDxVhHAAhotsk84Oy0ZPfBbEpCxF3PlDJMAfF4iU9KMDVmVIwRk435vRL1ONS5evMc3I4QSqj5nH4gQl52hoJdqXLGCGi_1FodbBn03P569PEpoAKeMHKRIoic_3Tj5CUKAXK1DJ3yJ8CYlHlIrXuIkrJcLmxg_RNXs_xMqBbl6wsKbiL6fNRyxpvZywZCZPGU948x3dcvvDnZPL8y17JpWNc8CwY_KG-2BuSEGTCiO_Rju_v_loTWwLQjB0eIihvfKCnMzOCR8U4-m6vtd3y59F1wEhr3ntjDs1VjjNqqcOUrlaSGtbUYpUim8WkAXMQJqqt0tsM1iNASh_adaBqZKYxZpDJzbGAd2lzy9Y9JvJALo9kCQ_3HlTLp5K1qE5mmgwNyF1xFedqbbJCr26bPoNBQ7dmquTapVqmSJcE3wjZ-nvkymNQSChzOpn7Uxl4ivwNQMs2UelqB)

---

<h1>Building blocks:</h1>

* `org.springframework.context.ConfigurableApplicationContext.refresh()`
* `org.springframework.cloud.context.config.annotation.@RefreshScope`
* actuator's `/refresh` endpoint (not used in autorefresh/push model, but might be useful)
* [Spring Cloud Config Server & Client](https://www.baeldung.com/spring-cloud-configuration)
* [Spring Cloud Bus](https://www.baeldung.com/spring-cloud-bus)
* Git server webhooks (triggering cloud refresh on git push)

---

<h1>Decisions:</h1>

* config storage backend (git|s3|jdbc|...)
* Config git repo layout
  * one repo for all apps and environments
  * one repo per app (all environments in one place)
  * one repo per environment (all apps in one place)
  * one repo per app/environment
* how much config shall live in config-server (runtime-toggles) vs. container configuration (startup-toggles) vs. local props (buildtime-toggles)
* bus transport binding (rabbit|kafka|sqs|...)

---

<h1>Further research:</h1>

* does refresh() work for ctor-injected props?
* would a backlog of RemoteRefreshEvents cause multiple unnecessary refreshing? (
  * i.e. if app starts after a period of downtime and bus has multiple pending RemoteRefreshEvents
* can config-server be backed up by AWS, to fetch props & observe changes?
  * Parameter Store? 
  * [App Config?](https://aws.amazon.com/blogs/mt/using-aws-appconfig-feature-flags/)