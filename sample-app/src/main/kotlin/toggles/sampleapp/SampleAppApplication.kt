package toggles.sampleapp

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

fun main(args: Array<String>) {
	runApplication<SampleAppApplication>(*args)
}

@SpringBootApplication
@RestController
@RefreshScope
class SampleAppApplication {

	@Value("\${pod.id:???}") lateinit var podID: String
	@Value("\${pod.env:???}") lateinit var podEnv: String

	@Value("\${features.greetings:nice}")
	lateinit var greetings: Greetings

	@GetMapping
	fun greet() = "$podID ($podEnv) says : ${greetings.text}"
}

@Suppress("unused")
enum class Greetings(val text: String) {
	nice("Hello my dear!"),
	angry("What the hell do you want?"),
	texan("Howdy, cowboy!")
}